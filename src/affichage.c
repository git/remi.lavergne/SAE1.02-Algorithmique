/**
 * \file    fonction1.c
 * \brief   Fonction de codage de contexte de l'information
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date    12 janvier 2024
*/

#include "specification1.h"

//~~~~~~~~~~~~PARTIE 1~~~~~~~~~~~~

/**
 * \brief                   Affiche les détails d'un devis
 * \struct[in]      devis   Le devis à afficher
*/
void afficherDevis(Devis devis)
{
    printf("####################################################\n");
    printf("Nom de l'enreprise : %s\n", devis.entreprise);
    printf("Adresse : %d, %s - %d - %s\n", devis.adresse.numero, devis.adresse.rue, devis.adresse.codePostal, devis.adresse.ville);
    printf("Capital : %d\n", devis.capital);
    printf("Durée estimée : %d jours\n", devis.duree);
    printf("Coût : %d€\n", devis.cout);
    printf("####################################################\n");
}

/**
* \brief 			Afficher l'ensemble des devis 
* \struct[out] 	*l  pointeur sur le maillonDevis
*/
void afficherListeDevis(ListeDevis l)
{
    if(l == NULL)
    {
        printf("Liste vide.\n");
    }
    else
    {
        afficherDevis(l->devis);
        afficherListeDevis(l->suiv);
    }
}

/**
* \brief 			Afficher le devis d'une entreprise donnée pour un type de travaux donnée
* \struct[in,out] 	tabTravaux      Tableau de pointeur de travaux
* \param[in]        "tlog"          Taille logique
* \param[in]        "entreprise"    Nom de l'entreprise
* \param[in]        "typeTravaux"   Type de travaux
*/
void afficherDevisEntreprise(TabTravaux tabTravaux, int tLog, char entreprise[], char typeTravaux[])
{
    for (int i = 0; i < tLog; i++)
    {
        if (strcmp(tabTravaux[i].travaux, typeTravaux) == 0)
        {
            ListeDevis listeDevis = tabTravaux[i].ldevis;

            while (listeDevis != NULL)
            {
                if (strcmp(listeDevis->devis.entreprise, entreprise) == 0)
                {
                    printf("=====================================\n");
                    printf("Type de travaux : %s\n", typeTravaux);
                    printf("Entreprise : %s\n", entreprise);
                    printf("=====================================\n");
                    afficherDevis(listeDevis->devis);
                }

                listeDevis = listeDevis->suiv;
            }
        }
    }
}

/**
* \brief 			        Afficher le devis des travaux
* \struct[out] 	*l          pointeur sur le maillonDevis
* \param[in]    "travaux"   nom de l'entreprise
*/
void afficherDevisTravaux(ListeDevis l, char travaux[])
{
    if(l == NULL)
    {
        printf("Liste vide.\n");
    }
    else
    {
        printf("=====================================\n");
        printf("Type de travaux : %s\n", travaux);
        printf("=====================================\n");
        if(strcmp(l->devis.entreprise, travaux) == 0)
            afficherDevis(l->devis);
        afficherDevisTravaux(l->suiv, travaux);
    }
}


/**
 * \brief                   Sélectionne l'entreprise retenue pour un type de travaux
 * \param[out]   "ldevis"   La liste des devis
*/
void selectionEntreprisesRetenues(ListeDevis* ldevis)
{
    if (*ldevis == NULL)
        return;

    triEchange(ldevis);

    MaillonDevis* actuel = (*ldevis)->suiv;
    while (actuel != NULL) // On libère la mémoire des devis non retenus (c'est à dire tous sauf le premier qui est celui qu'on veut)
    {
        MaillonDevis* suivant = actuel->suiv;
        free(actuel);
        actuel = suivant;
    }

    (*ldevis)->suiv = NULL;
}

/**
 * \brief                   Affiche les entreprises retenues pour chaque type de travaux
 * \param[in]       "tab"     Le tableau des travaux
 * \param[in]       "tLog"    La taille logique
*/
void afficherEntreprisesRetenues(TabTravaux tab, int tLog)
{
    for (int i = 0; i < tLog; i++)
    {
        printf("Type de travaux : %s\n", tab[i].travaux);
        afficherDevis(tab[i].ldevis->devis);
        printf("\n");
    }
}