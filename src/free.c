/**
 * \file 	free.c
 * \brief 	Liberer la mémoire de toutes le fonction
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date 	12 janvier 2024
*/

#include "specification1.h"

/**
 * \brief                          Liberer le tableau des travaux
 * \struct[in,out]    *tab         Tableau des travaux
 * \param[in]         "tLog"       Taille logique
*/
void libererTabTravaux(TabTravaux *tab, int tLog)
{
    for (int i = 0; i < tLog; i++)
    {
        libererOffre(&((*tab)[i]));
    }
    free(*tab);
}

/**
 * \brief                          Liberer l'offre
 * \struct[out]       *offre       Offre
*/
void libererOffre(Offre *offre)
{
    libererListeDevis(offre->ldevis);
}

/**
 * \brief                          Liberer la liste des devis
 * \struct[in]       liste         Liste des devis
*/
void libererListeDevis(ListeDevis liste)
{
    MaillonDevis *courant = liste;
    while (courant != NULL)
    {
        MaillonDevis *suivant = courant->suiv;
        free(courant);
        courant = suivant;
    }
}

// =======================================================

/**
 * \brief                          Liberer la mémoire allouée pour la liste de successeurs
 * \struct[out]       succ         Liste des seccesseur de la tache
*/
void libererListeSucc(Liste succ)
{
    while (succ != NULL)
    {
        MaillonSucc *temp = succ;
        succ = succ->suiv;
        free(temp);
    }
}

/**
 * \brief                          Liberer la mémoire allouée par la tache
 * \struct[out]       *tache       Tache
*/
void libererTache(Tache *tache)
{
    libererListeSucc(tache->succ);
    free(tache);
}

/**
 * \brief                             Liberer la mémoire allouée pour le tableau de taches
 * \struct[out]       **tabTaches     Tableau de pointeur de taches
 * \param[in]         "nbTaches"      Nombres de Taches
*/
void libererTabTaches(Tache **tabTaches, int nbTaches)
{
    for (int i = 0; i < nbTaches; i++)
    {
        libererTache(tabTaches[i]);
    }
    free(tabTaches);
}
