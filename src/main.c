/**
 * \file    main.c
 * \brief   Fonction globale
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date    12 janvier 2024
*/

#include "specification1.h"

/**
* \brief   Fonction menu  
*/
void menu(TabTravaux tabTravaux, int tLog, Tache  **tabTaches, int nbTaches)
{
        int rep;
        char typeTravaux[MAX_LENGTH], nomEntreprise[MAX_LENGTH];
        
        printf("¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤ MENU PRINCIPAL ¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤\n");
        printf("*\n");
        printf("¤Partie I : Contexte et codage des informations\n");
        printf("*-----------------------------------------------------------------------------------------\n");
        printf("¤ 1 - Afficher tout les devis.\n");
        printf("* 2 - Afficher l’ensemble des devis pour un type de travaux.\n");
        printf("¤ 3 - Afficher le devis d’une entreprise donnée pour un type de travaux donnée.\n");
        printf("*\n");
        printf("¤ Partie II : Sélection des entreprises retenues\n");
        printf("*-----------------------------------------------------------------------------------------\n");
        printf("¤ 4 - Afficher la meilleure entreprise par tâche");
        printf("*\n");
        printf("¤ Partie III : Nouvelle structure\n");
        printf("*------------------------------------------------------------------------------------------\n");
        printf("¤ 6 - ");
        printf("\n");
        printf(" Partie IV : Calcul de la date de début de chaque tâche et affichage des résultats\n");
        printf("-------------------------------------------------------------------------------------------\n");
        printf(" - Quitter\n");
        printf("¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤*¤\n");
        scanf("%d", &rep);

        switch (rep)
        {
            case 1:
                for (int i = 0; i < tLog; i++)
                    afficherListeDevis(tabTravaux[i].ldevis);
                break;
            case 2:
                printf("Quel type de travaux voulez-vous afficher ?\n");
                scanf("%s",typeTravaux); //! Vérifier que la chaine de caractère ne peut pas disposer pas d'espace
                
                int index = rechercheDichotomique(tabTravaux, tLog, typeTravaux);
                if (index == -1)
                    printf("Le type de travaux n'existe pas.\n");
                else
                    afficherDevisTravaux(tabTravaux[index].ldevis, typeTravaux);
                break;
            case 3:
                printf("Quel est le nom de l'entreprise ?\n");
                scanf("%s",nomEntreprise); //! Vérifier que la chaine de caractère ne peut pas disposer pas d'espace
                printf("Quel type de travaux voulez-vous afficher ?\n");
                scanf("%s",typeTravaux); //! Vérifier que la chaine de caractère ne peut pas disposer pas d'espace
                afficherDevisEntreprise(tabTravaux, tLog, nomEntreprise, typeTravaux);
                break;
            default:
                printf("Erreur de saisie.\n");
                break;
        }
}

/**
* \brief   Fonction global    
*/
void global(void)
{
    TabTravaux tab = NULL;
    int tLog = 0;

    char fichier[10] = "devis.txt";

    int result = chargement(&tab, &tLog, fichier);
    if (result < 0)
    {
        printf("[ERREUR] - Impossible de charger les travaux.\n");
        exit(100);
    }

    int nbTaches;
    Tache **tabTaches = chargementTaches(&tab, tLog, &nbTaches, "precedences.txt");
    if (tabTaches == NULL)
    {
        printf("[ERREUR] - Impossible de charger les tâches.\n");
        libererTabTravaux(&tab, tLog);
        exit(101);
    }

    menu(tab, tLog, tabTaches, nbTaches);

    sauvegardeDevisBinaire(tab, tLog, "devis_binaire.bin");
    sauvegardeTachesBinaire(tabTaches, nbTaches, "precedences_binaire.bin");
    
    sauvegardeTaches(tabTaches, nbTaches, "precedences.txt");
    sauvegardeDevis(tab, tLog, "devis.txt");

    libererTabTravaux(&tab, tLog);
    libererTabTaches(tabTaches, nbTaches);
}

int main(void)
{
    global();
    return 0;
}