/**
 * \file    chargement.c
 * \brief   Fonction de chargement
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date    12 janvier 2024
*/

#include "specification1.h"

/**
 * \brief                          Charge les données des devis d'un fichier dans l'Offre
 * \struct[out]        *tab        Le tableau des travaux
 * \param[in]          "fichier"   Le fichier à lire
 * \param[out]         *tLog       Taille logique
 * \return             0           Si tout s'est bien passé 
 * \return             1           En cas d'erreur d'ouverture
 * \return             2           En cas d'en-tête invalide)
*/
int chargement(TabTravaux *tab, int *tLog, char fichier[])
{
    FILE *flot;
    flot = fopen(fichier, "r");
    if(flot==NULL)
    {
        printf("[ERREUR] - Problème dans l'ouverture de %s en lecture !\n", fichier);
        return -1;
    }

    int tDevis;
    fscanf(flot, "%d", &tDevis); // Lecture de l'en-tête
    if(tDevis <= 0)
    {
        printf("[ERREUR] - Il n'y a pas de donnée à charger dans le fichier spécifié ou vous n'avez pas un en-tête valide.\n");
        fclose(flot);
        return -2;
    }

    *tLog = 0;

    for(int i = 0; i < tDevis; i++) {
        Devis d;
        char typeTravaux[MAX_LENGTH];
        lireDevis(flot, &d, typeTravaux);
        insererDevis(d, tab, tLog, typeTravaux);
        trierListeDevis(&(*tab)[i].ldevis);
    }

    fclose(flot);
    return 0;
}

/**
 * \brief                             Lis un seul devis et le stock dans la structure Devis
 * \struct[out]       *flot           Le fichier à lire
 * \struct[out]       *devis          La structure Devis dans laquelle stocker les données
 * \struct[out]       *typeTravaux    Le type de travaux
*/
void lireDevis(FILE *flot, Devis *devis, char *typeTravaux)
{
    char ligne[102];
    fscanf(flot, "%s", typeTravaux);
    fgets(devis->entreprise, 28, flot);

    fgets(ligne, sizeof(ligne), flot);
    // Utilisation de sscanf obligatoire pour découper la ligne pour la structure Adresse
    sscanf(ligne, "%d %[^0-9] %d %s", &devis->adresse.numero, devis->adresse.rue, &devis->adresse.codePostal, devis->adresse.ville);
    
    fscanf(flot, "%d", &devis->capital);
    fscanf(flot, "%d", &devis->duree);
    fscanf(flot, "%d", &devis->cout);
}

/**
 * \brief                         Inserer un nouveau Devis
 * \struct[in]      d             Le devis qu'on veut inserer
 * \struct[out]     *tab          La structure Devis dans laquelle stocker les données
 * \struct[out]     *tLog         Taille logique
 * \struct[in]      typeTravaux   Type de travaux
*/
void insererDevis(Devis d, TabTravaux *tab, int *tLog, char typeTravaux[])
{
    int index = rechercheDichotomique(*tab, *tLog, typeTravaux);

    if (index != -1)
    {
        // Le type de travaux existe déjà, insérer le devis dans la liste de devis associée
        MaillonDevis* nouveauMaillon = (MaillonDevis*)malloc(sizeof(MaillonDevis));
        if (nouveauMaillon == NULL) {
            printf("[ERREUR] - Échec de l'allocation mémoire pour le nouveau maillon.\n");
            exit(3);
        }

        nouveauMaillon->devis = d;
        nouveauMaillon->suiv = (*tab)[index].ldevis;
        (*tab)[index].ldevis = nouveauMaillon;
    } 
    else
    {
        if (*tab == NULL)
            *tab = (TabTravaux)malloc(1 * sizeof(Offre));
        else
        {
            TabTravaux aux = (TabTravaux)realloc(*tab, (*tLog+1) * sizeof(Offre));
            if (aux == NULL)
            {
                printf("[ERREUR] - Échec realloc.\n");
                exit(4);
            }
            *tab = aux;
        }
        if (*tab == NULL) {
            printf("[ERREUR] - Échec de l'allocation mémoire.\n");
            exit(2);
        }

        strcpy((*tab)[*tLog].travaux, typeTravaux);

        (*tab)[*tLog].ldevis = (MaillonDevis*)malloc(sizeof(MaillonDevis));
        if ((*tab)[*tLog].ldevis == NULL) {
            printf("[ERREUR] - Échec de l'allocation mémoire.\n");
            exit(2);
        }

        (*tab)[*tLog].ldevis->devis = d;
        (*tab)[*tLog].ldevis->suiv = NULL;
        (*tLog)++;
    }
}
