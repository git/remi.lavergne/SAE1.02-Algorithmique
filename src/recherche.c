/**
 * \file recherche.c
 * \brief Fonction de recherche
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date 12 janvier 2024
*/

#include "specification1.h"

/**
 * \brief                       Fonction de recherche dichotomique pour les Travaux
 * \struct[in]     "tab"        Le tableau des travaux
 * \param[in]      "tLog"       Taille logique
 * \struct[in]     typeTravaux  Le type de Travaux
 * \return         -1           Retourne quand il a pas trouvé
*/
int rechercheDichotomique(TabTravaux tab, int tLog, char typeTravaux[])
{
    int debut = 0;
    int fin = tLog;
    while (debut <= fin)
    {
        int milieu = (debut + fin) / 2;
        int cmp = strcmp(tab[milieu].travaux, typeTravaux);

        if (cmp == 0)
            return milieu;
        else if (cmp > 0)
            fin = milieu - 1;
        else
            debut = milieu + 1;
    }
    return -1;
}

/**
 * \brief                            Fonction de recherche dichotomique pour les taches
 * \struct[in,out]  **tabTaches      Le tableau de pointeur
 * \param[in]       "nbTaches"       Nombres de taches
 * \struct[in]      nomTache         Nom de la tache
 * \return          -1               Retourne quand il a pas trouvé
*/
int rechercheDichotomiqueTaches(Tache **tabTaches, int nbTaches, char nomTache[])
{
    int debut = 0;
    int fin = nbTaches - 1;

    while (debut <= fin)
    {
        int milieu = (debut + fin) / 2;

        int comparaison = strcmp(nomTache, tabTaches[milieu]->tache);

        if (comparaison == 0)
        {
            return milieu;
        }
        else if (comparaison < 0)
        {
            fin = milieu - 1;
        }
        else
        {
            debut = milieu + 1;
        }
    }

    return -1; // Tache non trouvée
}
