/**
 * \file    file.c
 * \brief   Fonction de files
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date    12 janvier 2024
*/

#include "specification1.h"

/**
 * \brief                          Initialiser la file d'attente
 * \return        nouvelleFile     Retourne une nouvelle file
 */
FileAttente initialiserFileAttente()
{
    FileAttente nouvelleFile;
    nouvelleFile.debut = NULL;
    nouvelleFile.fin = NULL;

    return nouvelleFile;
}

/**
 * \brief                         Vérifie si la file est vide
 * \param[in]     *file           File d'attente
 * \return        bool            Retourne un booléen
 */
bool estVide(FileAttente *file)
{
    return (file->debut == NULL);
}

/**
 * \brief                          Ajoute un élément à la file
 * \param[in,out] *file           File d'attente
 * \param[in]     *tache          Tache à ajouter
 */
void enfiler(FileAttente *file, Tache *tache)
{
    MaillonFile *nouveauMaillon = (MaillonFile *)malloc(sizeof(MaillonFile));
    if (nouveauMaillon == NULL)
    {
        printf("[ERREUR] - Problème d'allocation mémoire.\n");
        exit(1);
    }

    nouveauMaillon->tache = tache;
    nouveauMaillon->suiv = NULL;

    if (estVide(file))
    {
        file->debut = nouveauMaillon;
        file->fin = nouveauMaillon;
    }
    else
    {
        file->fin->suiv = nouveauMaillon;
        file->fin = nouveauMaillon;
    }
}

/**
 * \brief                          Retourne la tête de la file
 * \param[in]     *file           File d'attente
 * \return        *tache          Retourne la tache en tête de file
 */
Tache* tete(FileAttente *file)
{
    if (estVide(file))
    {
        printf("[ERREUR] - La file est vide.\n");
        exit(7);
    }

    return file->debut->tache;
}

/**
 * \brief                          Retire un élément de la file
 * \param[in,out] *file           File d'attente
 */
void defiler(FileAttente *file)
{
    if (estVide(file))
    {
        printf("[ERREUR] - La file est vide.\n");
        exit(7);
    }

    MaillonFile *tete = file->debut;
    file->debut = file->debut->suiv;
    free(tete);
}

/**
 * \brief                          Retourne la longueur de la file
 * \param[in]     *file           File d'attente
 * \return        longueur        Retourne la longueur de la file
 */
int longueur(FileAttente *file)
{
    int longueur = 0;
    MaillonFile *courant = file->debut;

    while (courant != NULL)
    {
        longueur++;
        courant = courant->suiv;
    }

    return longueur;
}

/**
 * \brief                          Affiche la file
 * \param[in]     *file           File d'attente
 */
void afficherFile(FileAttente *file)
{
    if (estVide(file))
    {
        printf("[ERREUR] - La file est vide.\n");
        exit(7);
    }

    MaillonFile *courant = file->debut;

    while (courant != NULL)
    {
        printf("%s ", courant->tache->tache);
        courant = courant->suiv;
    }
    printf("\n");
}

/**
 * \brief                          Détruit la file
 * \param[in,out] *file           File d'attente
 */
void detruireFile(FileAttente *file)
{
    while (!estVide(file))
    {
        defiler(file);
    }
}