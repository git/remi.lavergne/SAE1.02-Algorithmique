/**
 * \file    sauvegarde.c
 * \brief   Fonction de sauvegarde
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date    12 janvier 2024
*/

#include "specification1.h"

/**
* \brief                            Sauvegarde des taches dans precedences.txt
* \struct[in,out]   **tabTravaux    Tableau d'offre
* \param[in]        "nbTaches"      Nombres de taches
* \param[in]        "fichier"       Fichier de sauvegarde
*/
void sauvegardeTaches(Tache **tabTaches, int nbTaches, char fichier[])
{
    FILE *flot = fopen(fichier, "w");
    if (flot == NULL)
    {
        printf("[ERREUR] - Problème dans l'ouverture de %s en écriture !\n", fichier);
        return;
    }

    for (int i = 0; i < nbTaches; i++)
    {
        fprintf(flot, "%s\t", tabTaches[i]->tache);
        MaillonSucc *courant = tabTaches[i]->succ;
        while (courant != NULL)
        {
            fprintf(flot, "%s", courant->nomSucc);
            courant = courant->suiv;
            if (courant != NULL)
            {
                fprintf(flot, " ");
            }
        }
        fprintf(flot, "\n");
    }

    fclose(flot);
}

/**
* \brief                            Sauvegarde des devis dans devis
* \struct[in,out]   tab             Tableau de devis
* \param[in]        "tLog"          Taille logique
* \param[in]        "fichier"       Fichier de sauvegarde
*/
void sauvegardeDevis(TabTravaux tab, int tLog, char fichier[])
{
    FILE *flot = fopen(fichier, "w");
    if (flot == NULL)
    {
        printf("[ERREUR] - Problème dans l'ouverture de %s en écriture !\n", fichier);
        return;
    }

    fprintf(flot, "%d\n", tLog); // Écriture de l'en-tête
    for (int i = 0; i < tLog; i++)
    {
        ecrireDevis(flot, tab[i].travaux, tab[i].ldevis->devis);
        MaillonDevis *actuel = tab[i].ldevis->suiv;
        while (actuel != NULL)
        {
            ecrireDevis(flot, tab[i].travaux, actuel->devis);
            actuel = actuel->suiv;
        }
    }

    fclose(flot);
}

/**
 * \brief                            Écrit un seul devis dans un fichier
 * \struct[out]      *flot           Le fichier dans lequel écrire
 * \param[in]        *typeTravaux    Tableau de type de travaux
 * \struct[in]       devis           Le devis à écrire
*/
void ecrireDevis(FILE *flot, char typeTravaux[], Devis devis)
{
    fprintf(flot, "%s\n", typeTravaux);
    fprintf(flot, "%s\n", devis.entreprise);
    fprintf(flot, "%d %s - %d - %s\n", devis.adresse.numero, devis.adresse.rue, devis.adresse.codePostal, devis.adresse.ville);
    fprintf(flot, "%d\n", devis.capital);
    fprintf(flot, "%d\n", devis.duree);
    fprintf(flot, "%d\n", devis.cout);
}

// ==================== SAUVEGARDE BINAIRE ====================

/**
 * \brief                     Sauvegarde les devis dans un fichier binaire
 * \struct[in,out] *tab       Le tableau des travaux
 * \param[in]      "tLog"     Le nombre de devis à sauvegarder
 * \param[in]      "fichier"  Le fichier dans lequel sauvegarder
 */
void sauvegardeDevisBinaire(TabTravaux tab, int tLog, char fichier[])
{
    FILE *flot = fopen(fichier, "wb");
    if (flot == NULL)
    {
        printf("[ERREUR] - Problème dans l'ouverture de %s en écriture binaire !\n", fichier);
        return;
    }

    fwrite(&tLog, sizeof(int), 1, flot); // Écriture de l'en-tête

    for (int i = 0; i < tLog; i++)
    {
        fwrite(&tab[i].travaux, sizeof(char), 30, flot);

        MaillonDevis *actuel = tab[i].ldevis;
        while (actuel != NULL)
        {
            fwrite(&actuel->devis, sizeof(Devis), 1, flot);
            actuel = actuel->suiv;
        }
    }

    fclose(flot);
}

/**
 * \brief                           Sauvegarde les tâches dans un fichier binaire
 * \struct[in,out]  *tabTaches      Le tableau des tâches
 * \param[in]       "nbTaches"      Le nombre de tâches
 * \param[in]       "fichier"       Le fichier dans lequel sauvegarder
 */
void sauvegardeTachesBinaire(Tache **tabTaches, int nbTaches, char fichier[])
{
    FILE *flot = fopen(fichier, "wb");
    if (flot == NULL)
    {
        printf("[ERREUR] - Problème dans l'ouverture de %s en écriture binaire !\n", fichier);
        return;
    }

    fwrite(&nbTaches, sizeof(int), 1, flot); // Écriture de l'en-tête

    for (int i = 0; i < nbTaches; i++)
    {
        fwrite(&tabTaches[i], sizeof(Tache), 1, flot);
    }

    fclose(flot);
}
