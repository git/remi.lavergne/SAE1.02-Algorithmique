/**
 * \file    precedences.c
 * \brief   Gestion des taches et leurs précédences
 * \authors LAVERGNE Rémi ; DELINAC Inès ; RAYNAUD Camille
 * \date    12 janvier 2024
*/

#include "specification1.h"

// Fonction pour initialiser une tâche
/**
 * \brief                          Initialiser les taches
 * \param[out]    *nom            Nom de la tache
 * \param[in]     "duree"         Durée de la tache
 * \return        nouvelleTache   Retourne une nouvelle tache
*/
Tache* initialiserTache(char *nom, int duree)
{
    Tache *nouvelleTache = (Tache *)malloc(sizeof(Tache));
    if (nouvelleTache == NULL)
    {
        printf("[ERREUR] - Problème d'allocation mémoire.\n");
        exit(1);
    }

    strcpy(nouvelleTache->tache, nom);
    nouvelleTache->duree = duree;
    nouvelleTache->nbPred = 0;
    nouvelleTache->succ = NULL;
    nouvelleTache->dateDebut = 0;
    nouvelleTache->traite = false;

    return nouvelleTache;
}

// Fonction pour initialiser une tâche
/**
 * \brief                            Chargement des taches
 * \struct[in,out]    *tab           Tableau des travaux
 * \param[out]        *nbTaches      Nombres de Taches
 * \param[in]         "fichier"      Fichier
 * \return            tabTache       Retourne un tableau de tache
*/
Tache** chargementTaches(TabTravaux *tab, int tLog, int *nbTaches, char fichier[])
{
    FILE *flot;
    flot = fopen(fichier, "r");
    if (flot == NULL)
    {
        printf("[ERREUR] - Problème dans l'ouverture de %s en lecture !\n", fichier);
        exit(1);
    }

    // Initialisation du tableau des Taches
    *nbTaches = 0;
    Tache **tabTaches = NULL;

    // Lecture du fichier
    char tache1[20], tache2[20];

    fscanf(flot, "%s %s", tache1, tache2);
    while (!feof(flot))
    {
        // Recherche ou création des Taches
        int indexTache1 = rechercheDichotomiqueTaches(tabTaches, *nbTaches, tache1);
        int indexTache2 = rechercheDichotomiqueTaches(tabTaches, *nbTaches, tache2);

        if (indexTache1 == -1)
        {
            // Création et ajout de la Tache 1
            tabTaches = realloc(tabTaches, sizeof(Tache *) * (*nbTaches + 1));
            
            tabTaches[*nbTaches] = initialiserTache(tache1, recupererDuree(*tab, tLog, tache1));
            (*nbTaches)++;
            indexTache1 = *nbTaches - 1;
        }

        if (indexTache2 == -1)
        {
            // Création et ajout de la Tache 2
            tabTaches = realloc(tabTaches, sizeof(Tache *) * (*nbTaches + 1));
            tabTaches[*nbTaches] = initialiserTache(tache2, recupererDuree(*tab, tLog, tache2));
            (*nbTaches)++;
            indexTache2 = *nbTaches - 1;
        }

        // Ajout de la Tache 2 dans la liste des successeurs de la Tache 1
        ajouterSuccesseur(tabTaches[indexTache1], tache2);

        // Incrémentation du nbPred de la Tache 2
        tabTaches[indexTache2]->nbPred++;

        fscanf(flot, "%s %s", tache1, tache2);
    }

    fclose(flot);
    return tabTaches;
}

/**
 * \brief            Récupère la durée d'une tâche à partir du tableau des travaux
 * \struct[in,out]   *tabTravaux    Le tableau des travaux
 * \param[in]        "tLog"         Le nombre de travaux
 * \param[in]        "typeTravaux"  Le type de travaux à rechercher
 * \return            tabTravaux    La durée de la tâche 
 * \return            0             Si non trouvée
 */
int recupererDuree(TabTravaux tabTravaux, int tLog, char typeTravaux[])
{
    for (int i = 0; i < tLog; i++)
    {
        if (strcmp(tabTravaux[i].travaux, typeTravaux) == 0)
        {
            return tabTravaux[i].ldevis->devis.duree;
        }
    }
    return 0; // Tâche non trouvée
}

/**
 * \brief                       Ajoute un successeur à une tâche spécifique
 * \struct[out]     *tache      La tâche à laquelle ajouter un successeur
 * \param[in]       "nomSucc"   Le nom du successeur à ajouter
 */
void ajouterSuccesseur(Tache *tache, char nomSucc[])
{
    MaillonSucc *nouveauMaillon = (MaillonSucc *)malloc(sizeof(MaillonSucc));
    if (nouveauMaillon == NULL)
    {
        printf("[ERREUR] - Problème d'allocation mémoire.\n");
        exit(1);
    }

    strcpy(nouveauMaillon->nomSucc, nomSucc);
    nouveauMaillon->suiv = tache->succ;
    tache->succ = nouveauMaillon;

    tache->nbPred++;
}

